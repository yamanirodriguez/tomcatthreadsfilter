package com.yamani.tomcat.TomcatThreadFilter;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TomcatSpawnNewThreadFilter implements javax.servlet.Filter {

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain fc) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest)req;
        final Thread currentThread = Thread.currentThread();
        final String oldName = currentThread.getName();
        final String url = httpReq.getRequestURI();

        // Spawn new thread only if the url is not for retrieving js files
        if(!url.contains(".js")) {
            try {
                currentThread.setName(oldName + "-" + url);
                NonPoolThread nonPoolThread = new NonPoolThread(req, resp, fc);
                nonPoolThread.setName("SpawnedFromFilter-" + httpReq.getRequestURI());
                nonPoolThread.start();

                synchronized (nonPoolThread) {
                    try {
                        nonPoolThread.wait(); // Get lock
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // Reset threadName after execution completion of FilterChain;
                currentThread.setName(oldName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            fc.doFilter(req, resp);
        }
    }

    class NonPoolThread extends Thread {

        private boolean threadContextInheritable = false;
        private FilterChain filterChain;
        private HttpServletRequest request;
        private HttpServletResponse response;

        public NonPoolThread(ServletRequest req, ServletResponse resp, FilterChain fc) {
            this.filterChain = fc;
            this.request = (HttpServletRequest) req;
            this.response = (HttpServletResponse) resp;
        }

        @Override
        public void run() {
            synchronized (this) {
                //super.run();
                try {
                    ServletRequestAttributes attributes = new ServletRequestAttributes(request);
                    LocaleContextHolder.setLocale(request.getLocale(), this.threadContextInheritable);
                    RequestContextHolder.setRequestAttributes(attributes, this.threadContextInheritable);
                   //this.logger.debug("Bound request context to thread: " + request);

                    try {
                        filterChain.doFilter(request, response);
                    } finally {
                        LocaleContextHolder.resetLocaleContext();
                        RequestContextHolder.resetRequestAttributes();
                        attributes.requestCompleted();
                        // this.logger.debug("Cleared thread-bound request context: " + request);
                    }
                } catch(IllegalStateException e) {
                    e.printStackTrace();
                } catch(IOException|ServletException e) {
                    e.printStackTrace();
                } catch(Exception e) {
                    e.printStackTrace();
                }
                notify();
            }
        }
    }
}