package com.yamani.tomcat.TomcatThreadFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TomcatRenameThreadFilter implements javax.servlet.Filter {

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain fc) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest)req;
        final Thread currentThread = Thread.currentThread();
        final String oldName = currentThread.getName();

        // change the name of the current thread to something related
        // to the application (e.g. URI)
        try {
            currentThread.setName(oldName + "-" + httpReq.getRequestURI());
            fc.doFilter(req, resp);
        } catch(ServletException e){
            e.printStackTrace();
        }finally {
            currentThread.setName(oldName);
        }
    }
}